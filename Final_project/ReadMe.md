# Projet Spotify Analyse

## Pré-requis

Ce qu'il est requis pour commencer avec le projet :

- Python 3.x
- Linux ou Mac

## Installation (à faire une fois)

1. Télécharger le projet
 ```bash
 git clone https://gitlab.isima.fr/lomechoud/zz-3-integration-d-application.git
 ```


## Démarrage

Accéder au projet
 ```bash
 cd zz-3-integration-d-application/Final_project
 ```


Activer l'environnement virtuel pour les dépendances : 

 Linux/Mac:
 ```bash
 source app-env/bin/activate
 ```

Lancement du projet :

 Soyez dans le bon répertoire:
 ```bash
 cd mysite
 ```

 Exécutez la commande suivante:
 ```bash
 python manage.py runserver
 ```

 Ensuite, ouvrez un navigateur et accédez au lien suivant : [http://localhost:8000/home](http://localhost:8000/home)

Pour désactiver l'environnement virtuel, utilisez la commande :
 ```bash
 deactivate
 ```

## Programmes utilisés

Programmes/logiciels/ressources/outils utilisés pour développer le projet

* [Gitlab](https://gitlab.isima.fr) - Système de gestion de versions décentralisé
* [Django](https://www.djangoproject.com/) - Framework web Python (back-end)
* [Matplotlib](https://matplotlib.org/) - Bibliothèque de visualisation de données
* [Pandas](https://pandas.pydata.org/) - Bibliothèque de manipulation de données
* [Seaborn](https://seaborn.pydata.org/) - Bibliothèque de visualisation de données basée sur Matplotlib
* [scikit-learn](https://scikit-learn.org/) - Bibliothèque pour l'apprentissage automatique en Python


## Outils 

Pour notre projet, on a utilisé plusieurs outils tels que :

* [Gitlab](https://gitlab.isima.fr) - C'est un outil de gestion de la collaboration qui nous a permis d'avancer sur le projet simultanément et de fusionner les modifications plus facilement.
* [Django](https://www.djangoproject.com/) - C'est un framework pour le back-end de notre site qui nous a permis de mettre en œuvre le site avec sa gestion des URL et des modèles de données.
* [Jupyter](https://jupyter.org/) - C'est un environnement qui génère des graphiques. Cet outil nous a permis d'incorporer les graphiques dans Django plus facilement.




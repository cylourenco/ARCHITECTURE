from django.shortcuts import render, redirect
from django.http import HttpResponse
import matplotlib.pyplot as plt
from io import BytesIO
import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.ticker import ScalarFormatter
import seaborn as sns
import base64 
import numpy as np
from io import BytesIO
import base64
from sklearn.linear_model import LinearRegression
from spotipy import Spotify
import spotipy
from spotipy.oauth2 import SpotifyClientCredentials
from datetime import datetime
import os
from django.contrib import messages
import time as t

# Paramètres d'identification pour l'API Spotify
client_id = '1df45862a4d54ac78d46ef81561c2ae2'
client_secret = '2c35e52aeff14f4b813187f745430431'

# Crée une instance de connexion à l'API Spotify
client_credentials_manager = SpotifyClientCredentials(client_id=client_id, client_secret=client_secret)
sp = Spotify(client_credentials_manager=client_credentials_manager)


def get_graph():
    # Sauvegarde temporaire de l'image pour l'inclure dans le template HTML
    buffer = BytesIO()
    plt.savefig(buffer, format='png')
    buffer.seek(0)
    image_png = buffer.getvalue()
    image = base64.b64encode(image_png).decode('utf-8')
    buffer.close()
    return image

def read_csv_music_genre():
    # Recuperation des fichiers csv contenant les donnees
    df_evolution = pd.read_csv('mysite/data/evolution.csv')
    df_genre = pd.read_csv('mysite/data/music_genre.csv')

    # Mise en relation des deux tables
    mask = df_evolution['Track Name'].isin(df_genre['track_name'])
    df_evolution = df_evolution[mask]

    df_evolution = pd.merge(df_evolution, df_genre, left_on='Track Name', right_on='track_name', how='left')
    df_evolution = df_evolution.drop(columns='track_name')

    # Recuperation des annees dans une colonne a part
    df_evolution['Album Release Date'] = df_evolution['Album Release Date'].astype(str)
    df_evolution['Annee'] = df_evolution['Album Release Date'].str.extract(r'(\d{4})')

    # Conversion de la colonne 'Annee' en numérique
    df_evolution.loc[:, 'Annee'] = pd.to_numeric(df_evolution['Annee'], errors='coerce')
    df_evolution = df_evolution.dropna(subset=['Annee'])
    df_evolution['Annee'] = df_evolution['Annee'].astype('Int64')

    return df_evolution


def homepage_render_redirect(request):
    return redirect(homepage_render)



# Fonction associee a la page principale
def homepage_render(request):
    page = render(request, "homepage.html")
    return page



# Fonction associee a la page de redirection
def redirection_render(request):
    page = render(request, "redirection.html")
    return page



def ecriture_csv(playlist_url):


    # Extrait l'ID de la playlist à partir de l'URL
    playlist_id = playlist_url.split('/')[-1]

    # Teste la récupération des données de la playlist

    playlist_data = sp.playlist_tracks(playlist_id)

    # Lists to store data for each CSV
    music_genre_data = []
    best_song_data = []
    evolution_data = []
    spotify_2023_data = []



    for item in playlist_data['items']:
        track = item['track']
        track_id = track['id']
        track_name = track['name']
        artist_names = ', '.join([artist['name'] for artist in track['artists']])
        album = track['album']
        release_date = album['release_date'].split('-')

        print(track_id)
        # Fetch track's audio features
        audio_features = sp.audio_features(track_id)[0]

        print(audio_features)

        # Fetch artist details to extract genre information
        artist_info = sp.artist(track['artists'][0]['id'])
        artist_genres = ', '.join(artist_info['genres'])


        # Prepare data for music_genre.csv
        music_genre_data.append({
            'instance_id': track_id,
            'artist_name': artist_names,
            'track_name': track_name,
            'popularity': track['popularity'],
            'acousticness': audio_features['acousticness'],
            'danceability': audio_features['danceability'],
            'duration_ms': audio_features['duration_ms'],
            'energy': audio_features['energy'],
            'instrumentalness': audio_features['instrumentalness'],
            'key': audio_features['key'],
            'liveness': audio_features['liveness'],
            'loudness': audio_features['loudness'],
            'mode': audio_features['mode'],
            'speechiness': audio_features['speechiness'],
            'tempo': audio_features['tempo'],
            'obtained_date': datetime.now().strftime('%Y-%m-%d'),
            'valence': audio_features['valence'],
            'music_genre': artist_genres
        })

        # Prepare data for best-song.csv
        best_song_data.append({
            'title': track_name,
            'artist': artist_names,
            'top genre': artist_genres,
            'year': release_date[0] if release_date else '',
            'bpm': audio_features['tempo'],
            'energy': audio_features['energy'] * 100,
            'danceability': audio_features['danceability'] * 100,
            'dB': audio_features['loudness'],
            'liveness': audio_features['liveness'] * 100,
            'valence': audio_features['valence'] * 100,
            'duration': int(audio_features['duration_ms'] / 1000),
            'acousticness': audio_features['acousticness'] * 100,
            'speechiness': audio_features['speechiness'] * 100,
            'popularity': track['popularity']
        })

        # Prepare data for evolution.csv
        evolution_data.append({
            "Track URI": track['uri'],
            "Track Name": track_name,
            "Artist URI(s)": ', '.join([artist['uri'] for artist in track['artists']]),
            "Artist Name(s)": artist_names,
            "Album URI": album['uri'],
            "Album Name": album['name'],
            "Album Artist URI(s)": ', '.join([artist['uri'] for artist in album['artists']]),
            "Album Artist Name(s)": ', '.join([artist['name'] for artist in album['artists']]),
            "Album Release Date": album['release_date'],
            "Album Image URL": album['images'][0]['url'] if album['images'] else '',
            "Disc Number": track['disc_number'],
            "Track Number": track['track_number'],
            "Track Duration (ms)": track['duration_ms'],
            "Track Preview URL": track['preview_url'],
            "Explicit": track['explicit'],
            "Popularity": track['popularity'],
            "ISRC": track['external_ids'].get('isrc', ''),
            "Added At": datetime.now().isoformat(),
            "Artist Genres": artist_genres,
            "Danceability": audio_features['danceability'],
            "Energy": audio_features['energy'],
            "Key": audio_features['key'],
            "Loudness": audio_features['loudness'],
            "Mode": audio_features['mode'],
            "Speechiness": audio_features['speechiness'],
            "Acousticness": audio_features['acousticness'],
            "Instrumentalness": audio_features['instrumentalness'],
            "Liveness": audio_features['liveness'],
            "Valence": audio_features['valence'],
            "Tempo": audio_features['tempo'],
            "Time Signature": audio_features['time_signature']
        })

        # Prepare data for soptify-2024.csv
        spotify_2023_data.append({
            'track_name': track_name,
            'artist(s)_name': artist_names,
            'artist_count': len(track['artists']),
            'released_year': release_date[0] if release_date else '',
            'released_month': release_date[1] if len(release_date) > 1 else '',
            'released_day': release_date[2] if len(release_date) > 2 else '',
            'bpm': audio_features['tempo'],
            'key': audio_features['key'],
            'mode': 'Major' if audio_features['mode'] == 1 else 'Minor',
            'danceability_%': audio_features['danceability'] * 100,
            'valence_%': audio_features['valence'] * 100,
            'energy_%': audio_features['energy'] * 100,
            'acousticness_%': audio_features['acousticness'] * 100,
            'instrumentalness_%': audio_features['instrumentalness'] * 100,
            'liveness_%': audio_features['liveness'] * 100,
            'speechiness_%': audio_features['speechiness'] * 100
        })
    

    # Save each dataset to its corresponding CSV file


    pd.DataFrame(music_genre_data).to_csv('mysite/data/music_genre.csv', index=False)
    pd.DataFrame(best_song_data).to_csv('mysite/data/best-song.csv', sep=',', index=False)
    pd.DataFrame(evolution_data).to_csv('mysite/data/evolution.csv', index=False)
    pd.DataFrame(spotify_2023_data).to_csv('mysite/data/spotify-2023.csv', sep=',', index=False)



def connexion(request):
    if request.method == 'POST':
        # Récupère le lien de la playlist depuis le formulaire
        playlist_url = request.POST.get('playlist_url')

        try:

            # ecriture des données relative à la playlist
            # ecriture_csv(playlist_url)
            t.sleep(3)

            # Si la playlist est récupérée avec succès, redirige vers la page d'analyse
            return redirect(redirection_render)
        
        except Exception as e:
            # Si une erreur survient (ex: playlist non trouvée), affiche un message d'erreur
            messages.error(request, "Mauvaise URL de playlist. Veuillez réessayer.")

            # Si une erreur survient (ex: playlist non trouvée), renvoie sur la page d'accueil avec un message d'erreur
            return redirect(homepage_render)

    # Si la méthode n'est pas POST, renvoie l'utilisateur à la page d'accueil
    return redirect('/')



# Fonction associee a la page de revue de la playlist
def overview(request):

    matplotlib.use('Agg')

    try:
        best_song_df = pd.read_csv('mysite/data/best-song.csv')
        evolution_df = pd.read_csv('mysite/data/evolution.csv')
        music_genre_df = pd.read_csv('mysite/data/music_genre.csv')
        spotify_2023_df = pd.read_csv('mysite/data/spotify-2023.csv')

    except pd.errors.ParserError as e:
        print(f"Erreur de lecture du fichier CSV : {e}")

    # Data Cleanup and Preparation (adjusting columns as needed)
    # Depending on the separator used in your CSV, you may need to specify the separator (e.g., sep=";")
    best_song_df.columns = best_song_df.columns.str.strip()  # Strip any leading/trailing whitespaces
    music_genre_df.columns = music_genre_df.columns.str.strip()

    # If columns like 'artist_name' and 'music_genre' contain multiple items separated by a comma, keep only the first item
    def keep_first_item(cell):
        if isinstance(cell, str) and ',' in cell:
            return cell.split(',')[0].strip()
        return cell

    best_song_df['artist'] = best_song_df['artist'].apply(keep_first_item)
    best_song_df['top genre'] = best_song_df['top genre'].apply(keep_first_item)
    music_genre_df['music_genre'] = music_genre_df['music_genre'].apply(keep_first_item)

    # Merge datasets on common keys (assuming 'track_name' or 'spotify:track' as identifiers)
    merged_df = pd.merge(best_song_df, music_genre_df, left_on='title', right_on='track_name', how='left')

    # Overview Statistics
    total_tracks = merged_df['title'].nunique()
    unique_artists = merged_df['artist'].nunique()
    unique_genres = merged_df['music_genre'].nunique()

    # Aggregating by artist and genre
    artist_distribution = merged_df.groupby('artist').size().sort_values(ascending=False)
    genre_distribution = merged_df.groupby('music_genre').size().sort_values(ascending=False)

    # Calculating the average duration in minutes
    #merged_df['duration_minutes'] = merged_df['duration_ms'] / 60000  # Convert milliseconds to minutes
    #average_duration = merged_df['duration_minutes'].mean()

    context = {}

    # Graphique
    fig, ax = plt.subplots(figsize=(6, 6))  # Augmentation de la taille pour un affichage plus clair

    # Camembert - Répartition des artistes
    top_artists = artist_distribution.head(10)  # Top 10 artistes par nombre de morceaux
    wedges, texts, autotexts = ax.pie(
        top_artists, 
        labels=top_artists.index, 
        autopct='%1.1f%%', 
        startangle=40,
        pctdistance=0.85  # Ajuste la position du pourcentage pour qu'il soit plus proche du centre
    )

    # Amélioration de la lisibilité du texte
    for text in texts:
        text.set_fontsize(10)  # Taille de police des étiquettes des artistes
    for autotext in autotexts:
        autotext.set_fontsize(10)  # Taille de police des pourcentages

    ax.set_title('Top 10 des Artistes les plus écoutés', fontsize=14, fontweight='bold')

    # Recuperation du graphique sous format image (adaptee pour le site)
    image_top_artists = BytesIO()
    plt.savefig(image_top_artists, format='png')
    image_top_artists.seek(0)
    img_top_artists = base64.b64encode(image_top_artists.getvalue()).decode('utf-8')
    plt.close()
    context['img_top_artists'] = img_top_artists


    # Diagramme en barres horizontales
    fig, ax = plt.subplots(figsize=(8, 6))  # Taille augmentée pour meilleure lisibilité

    # Données des top 10 genres
    top_genres = genre_distribution.head(10)

    # Couleurs personnalisées
    colors = plt.cm.tab10(range(len(top_genres)))

    # Création du graphique en barres horizontales
    ax.barh(
        top_genres.index[::-1],  # Inverser les labels pour afficher du plus grand au plus petit
        top_genres.values[::-1],  # Même ordre que les labels
        color=colors, edgecolor='black', alpha=0.8
    )

    # Ajouter les annotations sur les barres
    for i, (genre, count) in enumerate(zip(top_genres.index[::-1], top_genres.values[::-1])):
        ax.text(
            count + 0.5,  # Position légèrement à droite de la barre
            i,  # Position sur l'axe vertical
            f"{count}",  # Texte (nombre de morceaux)
            va='center', ha='left', fontsize=10, color='black'
        )

    # Titre et labels
    ax.set_title('Top 10 des Genres les Plus Écoutés', fontsize=16, fontweight='bold', color='#333')
    ax.set_xlabel('Nombre de Morceaux', fontsize=12, labelpad=10)
    ax.set_ylabel('Genres Musicaux', fontsize=12, labelpad=10)

    # Style de l'axe
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_linewidth(0.5)
    ax.spines['bottom'].set_linewidth(0.5)

    ax.tick_params(axis='y', labelsize=12)
    ax.tick_params(axis='x', labelsize=10)
    ax.xaxis.grid(True, linestyle='--', alpha=0.6)

    # Sauvegarde de l'image dans un format lisible pour le contexte
    image_top_genres = BytesIO()
    plt.savefig(image_top_genres, format='png', bbox_inches='tight', dpi=150)
    image_top_genres.seek(0)
    img_top_genres = base64.b64encode(image_top_genres.getvalue()).decode('utf-8')
    plt.close(fig)

    context['img_top_genres'] = img_top_genres

    #---------------------------------------------------
    # Duree des morceaux

    best_song_df['duration_minutes'] = best_song_df['duration'] / 60  # Conversion en minutes

    duration_distribution = best_song_df.groupby('top genre')['duration_minutes'].mean().sort_values(ascending=False)


    top_genres = duration_distribution.head(10)

    # Création du graphique
    fig, ax = plt.subplots(figsize=(8, 5))
    top_genres.plot(kind='bar', color='teal', ax=ax)

    # Ajout des labels et du titre
    ax.set_xlabel('Genre')
    ax.set_ylabel('Durée Moyenne (minutes)')
    ax.set_title('Durée Moyenne des Morceaux par Genre')
    plt.xticks(rotation=45, ha='right')

    # Sauvegarde de l'image dans un format lisible pour le contexte
    img_average_duration = BytesIO()
    plt.savefig(img_average_duration, format='png', bbox_inches='tight')  # bbox_inches ajuste les marges pour une bonne visibilité
    img_average_duration.seek(0)
    img_average_duration = base64.b64encode(img_average_duration.getvalue()).decode('utf-8')
    plt.close(fig)

    context['img_average_duration'] = img_average_duration

    
    return render(request, 'overview.html', context)

# Fonction associee a la page de l'actualite de la musique
def energy(request):
    try:
        best_song_df = pd.read_csv('mysite/data/best-song.csv')
        evolution_df = pd.read_csv('mysite/data/evolution.csv')
        music_genre_df = pd.read_csv('mysite/data/music_genre.csv')
        spotify_2023_df = pd.read_csv('mysite/data/spotify-2023.csv')
    except pd.errors.ParserError as e:
        print(f"Erreur de lecture du fichier CSV : {e}")

    # Clean up data
    best_song_df.columns = best_song_df.columns.str.strip()
    music_genre_df.columns = music_genre_df.columns.str.strip()

    # Keep only the first item if there are multiple items in a cell
    def keep_first_item(cell):
        if isinstance(cell, str) and ',' in cell:
            return cell.split(',')[0].strip()
        return cell

    best_song_df['artist'] = best_song_df['artist'].apply(keep_first_item)
    music_genre_df['music_genre'] = music_genre_df['music_genre'].apply(keep_first_item)

    # Merge data
    merged_df = pd.merge(best_song_df, music_genre_df, left_on='title', right_on='track_name', how='left')

    genre_distribution = merged_df.groupby('music_genre').size().sort_values(ascending=False)

    # Average of each audio feature
    audio_features = ['danceability', 'energy', 'valence', 'acousticness', 'liveness', 'speechiness']
    average_features = music_genre_df[audio_features].mean()

    ###########Radar graph
    title = "Analyse en toile des caractéristiques musicales"

    # Number of variables we're plotting
    num_vars = len(audio_features)


    # Split the circle into equal parts and create angles for each axis
    angles = np.linspace(0, 2 * np.pi, num_vars, endpoint=False).tolist()

    # The plot is a circular loop, so close the loop by appending the first value
    values = average_features.tolist()
    values += values[:1]
    angles += angles[:1]

    # Set up the radar chart
    fig, ax = plt.subplots(figsize=(6, 6), subplot_kw=dict(polar=True))
    #fig, ax = plt.subplots(figsize=(6, 6))

    # Draw the outline of the radar chart
    ax.fill(angles, values, color='teal', alpha=0.25)
    ax.plot(angles, values, color='teal', linewidth=2)

    # Add labels for each axis (the audio feature names)
    ax.set_yticklabels([])  # No radial labels
    ax.set_xticks(angles[:-1])  # Set the ticks to the angles corresponding to features
    ax.set_xticklabels(audio_features)  # Label each axis with the feature name

    # Title of the radar chart
    plt.title(title, fontsize=14, fontweight='bold', y=1.05)

    # Annotating the values on the radar chart
    for i in range(len(average_features)):
        #ax.text(angles[i], values[i] + 0.05, f"{audio_features[i]}: {values[i]:.2f}", horizontalalignment='center', size=10, color='teal')
        ax.text(angles[i], values[i] + 0.05, f"{values[i]:.2f}", horizontalalignment='center', size=10, color='teal')

    # Save the radar chart to a BytesIO object
    img_radar = BytesIO()
    plt.savefig(img_radar, format='png')
    img_radar.seek(0)

    # Convert the image to base64
    img_radar = base64.b64encode(img_radar.getvalue()).decode('utf-8')

    # Close the plot to avoid showing it interactively
    plt.close(fig)

    # Store the image in the context for later use
    
    context = {'img_radar': img_radar}
    
    ######################## bar chart

    # Average of Danceability, Energy, and Valence
    audio_features = ['danceability', 'energy', 'valence']
    average_features = music_genre_df[audio_features].mean()

    ########### Bar chart
    title = "Average Danceability, Energy, and Valence"

    # Plot the bar chart
    fig, ax = plt.subplots(figsize=(8, 6))

    # Create the bar chart
    ax.bar(average_features.index, average_features.values, color=['#66b3ff', '#ff9966', '#66ff66'])

    # Add labels and title
    ax.set_xlabel('Audio Features', fontsize=12)
    ax.set_ylabel('Average Value', fontsize=12)
    ax.set_title(title, fontsize=14, fontweight='bold')

    # Annotating the values on top of each bar
    for i, value in enumerate(average_features):
        ax.text(i, value + 0.02, f"{value:.2f}", ha='center', fontsize=10, color='black')

    # Save the bar chart to a BytesIO object
    img_bar = BytesIO()
    plt.savefig(img_bar, format='png')
    img_bar.seek(0)

    # Convert the image to base64
    img_bar = base64.b64encode(img_bar.getvalue()).decode('utf-8')

    # Close the plot to avoid showing it interactively
    plt.close()

    # Add the image and other context to be returned
    context['img_bar'] = img_bar
    #context = {
    #    'img_bar': img_bar,  # The base64 image to be used in the context (for rendering in a webpage)
    #    'average_features': average_features.to_dict()  # Optional, to show the average values for reference
    #}
    ################# Line energy evolution chart

    best_song_df['artist'] = best_song_df['artist'].apply(keep_first_item)
    music_genre_df['music_genre'] = music_genre_df['music_genre'].apply(keep_first_item)
    print("popularity")
    print(best_song_df['popularity'][1])

    # Merge data to have track information alongside their features
    merged_df = pd.merge(best_song_df, music_genre_df, left_on='title', right_on='track_name', how='left')

    # Sort the data by popularity (assuming there's a column for track order like 'track_order' or 'track_position')

    print("Columns in merged_df:")
    print(merged_df.columns.tolist())

    print("popularity2")
    print(merged_df['popularity_x'][1])
    merged_df = merged_df.sort_values(by='bpm')  # Replace 'track_position' with your actual column if different

    # Extract energy values for plotting
    energy_values = merged_df['energy_x'].tolist()

    # Create track identifiers (or use a track name/ID from the dataset for x-axis labels)
    track_ids = merged_df['bpm'].tolist()

    ########### Line Graph: Energy Flow
    title = "Flux d'Énergie à Travers la Playlist"

    # Plot the line graph
    fig, ax = plt.subplots(figsize=(10, 6))

    ax.plot(track_ids, energy_values, marker='o', color='teal', linewidth=2, markersize=5)

    # Add labels and title
    ax.set_xlabel('Popularité', fontsize=12)
    ax.set_ylabel('Energy Value', fontsize=12)
    ax.set_title(title, fontsize=15, color='teal')

    # Rotate x-axis labels for better readability (if there are many tracks)
    plt.xticks(rotation=90, fontsize=10)

    # Save the line chart to a BytesIO object
    image_energy_evolution = BytesIO()
    plt.savefig(image_energy_evolution, format='png')
    image_energy_evolution.seek(0)

    # Convert the image to base64
    img_energy_evolution = base64.b64encode(image_energy_evolution.getvalue()).decode('utf-8')

    # Close the plot to avoid showing it interactively
    plt.close()

    # Add the image and other context to be returned

    ##context = {
      #  'img_energy_evolution': img_energy_evolution,  # The base64 image to be used in the context (for rendering in a webpage)
       # 'track_ids': track_ids,  # Optional: return track names/IDs for reference
        #'energy_values': energy_values  # Optional: return the energy values for reference
    #}
    
    ################Pie Chart - Genre Distribution
    context = {
        'img_bar': img_bar,  # The base64 image to be used in the context (for rendering in a webpage)
        'average_features': average_features.to_dict()  # Optional, to show the average values for reference
    }



    #################Pie Chart - Genre Distribution
        
    # Configuration de la figure et du graphique
    fig, ax = plt.subplots(figsize=(6, 6))  # Augmentation de la taille pour un affichage plus clair

    # Camembert - Répartition des genres
    top_genres = genre_distribution.head(10)  # Top 10 genres par nombre de morceaux
    wedges, texts, autotexts = ax.pie(
        top_genres, 
        labels=top_genres.index, 
        autopct='%1.1f%%', 
        startangle=140,
        pctdistance=0.85  # Ajuste la position des pourcentages pour les rapprocher du centre
    )

    # Amélioration de la lisibilité du texte
    for text in texts:
        text.set_fontsize(10)  # Taille de police des étiquettes de genres
    for autotext in autotexts:
        autotext.set_fontsize(10)  # Taille de police des pourcentages

    ax.set_title('Top 10 des Genres les plus écoutés', fontsize=14, fontweight='bold')

    # Sauvegarde de l'image dans un format lisible pour le contexte
    image_top_genres = BytesIO()
    plt.savefig(image_top_genres, format='png', bbox_inches='tight')  # bbox_inches ajuste les marges pour une bonne visibilité
    image_top_genres.seek(0)
    img_top_genres = base64.b64encode(image_top_genres.getvalue()).decode('utf-8')
    plt.close(fig)

    context['img_top_genres'] = img_top_genres
    context['img_radar'] = img_radar
    context['img_energy_evolution'] = img_energy_evolution


    '''
    # Comparaison par artiste
    columns_of_interest = ['artist(s)_name', 'streams']
    df_artists_streams = df_spotify_2023[columns_of_interest]

    df_artists_streams['streams'] = df_artists_streams['streams'].replace({',': ''}, regex=True)
    df_artists_streams['streams'] = pd.to_numeric(df_artists_streams['streams'], errors='coerce')

    df_top_artists = df_artists_streams.groupby('artist(s)_name').sum()

    df_top_artists = df_top_artists.sort_values(by='streams', ascending=False)

    # Top 10
    top_10_artists_streams = df_top_artists.head(10)
    
    # Graphique
    plt.figure(figsize=(10, 6))
    sns.barplot(x=top_10_artists_streams.index, y=top_10_artists_streams['streams'], palette= 'magma')
    plt.title('Top 10 Artists by Total Streams')
    plt.xlabel('Artist')
    plt.ylabel('Total Streams')
    plt.xticks(rotation=45, ha='right')
    plt.tight_layout()

    # Convertion en image
    image_top_artists = BytesIO()
    plt.savefig(image_top_artists, format='png')
    image_top_artists.seek(0)
    img_data_top_artists = base64.b64encode(image_top_artists.getvalue()).decode('utf-8')
    plt.close()
    context['img_data_top_artists'] = img_data_top_artists


    # Graphique pour le top 10 des artistes
    plt.figure(figsize=(9, 5))
    sns.barplot(x='Nombre', y='Artiste', data=top_10_artistes, palette='magma')
    plt.title('Top 10 des Artistes avec le Plus Grand Nombre de Chansons')
    plt.xlabel('Nombre de Chansons')
    plt.ylabel('Artiste')
    
    image_artists = BytesIO()
    plt.savefig(image_artists, format='png')
    image_artists.seek(0)
    img_data_artists = base64.b64encode(image_artists.getvalue()).decode('utf-8')
    plt.close()
    context['img_data_artists'] = img_data_artists

    # Fusion des deux DataFrames sur le nom de l'artiste pour obtenir le genre
    df = pd.merge(df_spotify_2023, df_best_songs, left_on='artist(s)_name', right_on='artist', how='inner')

    # Meilleurs genres en fonction de la popularité dans les charts Spotify
    top_genres = df.groupby('top genre')['in_spotify_charts'].sum().nlargest(10).reset_index()

    # Graphique pour les meilleurs genres
    plt.figure(figsize=(12, 6))
    sns.barplot(x='top genre', y='in_spotify_charts', data=top_genres, palette='magma')
    plt.title('Top 10 des Meilleurs Genres sur Spotify')
    plt.xlabel('Genre')
    plt.ylabel('Popularité dans les Charts Spotify')
    plt.xticks(rotation=45, ha='right')
    plt.tight_layout()

    # Enregistrement de l'image
    image_genres = BytesIO()
    plt.savefig(image_genres, format='png')
    image_genres.seek(0)
    img_data_genres = base64.b64encode(image_genres.getvalue()).decode('utf-8')
    plt.close()
    context['img_data_genres'] = img_data_genres

    # Graphique pour les clés
    plt.figure(figsize=(12, 8))
    sns.boxplot(x='key', y='in_spotify_charts', data=df, palette='magma')
    plt.title('Top Hits : Distribution de la Popularité par Clé Musicale')
    plt.xlabel('Clé Musicale')
    plt.ylabel('Popularité dans les Charts Spotify')
    plt.tight_layout()

    # Enregistrement de l'image
    image_boxplot_keys = BytesIO()
    plt.savefig(image_boxplot_keys, format='png')
    image_boxplot_keys.seek(0)
    img_data_boxplot_keys = base64.b64en        ###########Radar graph
    title = "Radar Chart Example"  # Example title

    # Number of variables we're plotting (number of audio features)
    num_vars = len(features)

    # Split the circle into equal parts and create angles for each axis
    angles = np.linspace(0, 2 * np.pi, num_vars, endpoint=False).tolist()

    # The plot is a circular loop, so we need to "close the loop" and append the first value to the end
    values = values.tolist()
    values += values[:1]
    angles += angles[:1]

    # Set up the radar chart
    fig, ax = plt.subplots(figsize=(6, 6), subplot_kw=dict(polar=True))

    # Draw the outline of the radar chart
    ax.fill(angles, values, color='teal', alpha=0.25)
    ax.plot(angles, values, color='teal', linewidth=2)

    # Add labels for each axis
    ax.set_yticklabels([])  # Remove the radial labels (circle values)
    ax.set_xticks(angles[:-1])
    ax.set_xticklabels(features)

    # Title of the radar chart
    plt.title(title, size=15, color='teal', y=1.1)

    # Save the radar chart to a BytesIO object to convert it to an image format
    image_radar = BytesIO()
    plt.savefig(image_radar, format='png')
    image_radar.seek(0)

    # Convert the image to base64 encoding
    img_radar = base64.b64encode(image_radar.getvalue()).decode('utf-8')

    # Close the plot to avoid showing it interactively
    plt.close()

    # Assuming you want to store it in a context dictionary for later use
    context = {}
    context['img_radar'] = img_radarsis
    columns_of_interest = ['track_name', 'bpm', 'streams']
    df_tempo_streams = df_spotify_2023[columns_of_interest]

    # Convert the 'streams' column to numeric, handling the commas in the numbers
    df_tempo_streams['streams'] = df_tempo_streams['streams'].replace({',': ''}, regex=True)
    df_tempo_streams['streams'] = pd.to_numeric(df_tempo_streams['streams'], errors='coerce')

    # Drop rows with missing values in 'bpm' or 'streams'
    df_tempo_streams = df_tempo_streams.dropna(subset=['bpm', 'streams'])

    # Plotting the distribution plot
    plt.figure(figsize=(12, 6))
    sns.scatterplot(x='bpm', y='streams', data=df_tempo_streams, alpha=0.5, palette='magma')
    plt.title('Distribution of Tempo (BPM) vs. Streams')
    plt.xlabel('Tempo (BPM)')
    plt.ylabel('Streams')

    # Enregistrement de l'image
    image_distrib_tempos = BytesIO()
    plt.savefig(image_distrib_tempos, format='png')
    image_distrib_tempos.seek(0)
    img_data_distrib_tempos = base64.b64encode(image_distrib_tempos.getvalue()).decode('utf-8')
    plt.close()
    context['img_data_distrib_tempos'] = img_data_distrib_tempos

    # Calculate the correlation matrix
    corr_matrix = df_spotify_2023.corr(numeric_only= True)

    # Plot the correlation matrix using Seaborn heatmap
    plt.figure(figsize=(12, 12))
    sns.heatmap(corr_matrix, annot=True, cmap=sns.cubehelix_palette(as_cmap=True), fmt='.2f', linewidths=.5)
    plt.title('Matrice de Correlation entre Streams et toutes les autres caracteristiques musicales')

    # Enregistrement de l'image
    image_corr = BytesIO()
    plt.savefig(image_corr, format='png')
    image_corr.seek(0)
    img_data_corr = base64.b64encode(image_corr.getvalue()).decode('utf-8')
    plt.close()
    context['img_data_corr'] = img_data_corr
    '''
    
    return render(request, 'energy.html', context)
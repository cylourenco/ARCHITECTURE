from django.shortcuts import render
from django.http import HttpResponse
import matplotlib.pyplot as plt
from io import BytesIO
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import ScalarFormatter
import seaborn as sns
import base64 
import numpy as np
from io import BytesIO
import base64
from sklearn.linear_model import LinearRegression


def get_graph():
    # Sauvegarde temporaire de l'image pour l'inclure dans le template HTML
    buffer = BytesIO()
    plt.savefig(buffer, format='png')
    buffer.seek(0)
    image_png = buffer.getvalue()
    image = base64.b64encode(image_png).decode('utf-8')
    buffer.close()
    return image

def read_csv_music_genre():
    # Recuperation des fichiers csv contenant les donnees
    df_evolution = pd.read_csv('mysite/data/evolution.csv')
    df_genre = pd.read_csv('mysite/data/music_genre.csv')

    # Mise en relation des deux tables
    mask = df_evolution['Track Name'].isin(df_genre['track_name'])
    df_evolution = df_evolution[mask]

    df_evolution = pd.merge(df_evolution, df_genre, left_on='Track Name', right_on='track_name', how='left')
    df_evolution = df_evolution.drop(columns='track_name')

    # Recuperation des annees dans une colonne a part
    df_evolution['Album Release Date'] = df_evolution['Album Release Date'].astype(str)
    df_evolution['Annee'] = df_evolution['Album Release Date'].str.extract(r'(\d{4})')

    # Conversion de la colonne 'Annee' en numérique
    df_evolution.loc[:, 'Annee'] = pd.to_numeric(df_evolution['Annee'], errors='coerce')
    df_evolution = df_evolution.dropna(subset=['Annee'])
    df_evolution['Annee'] = df_evolution['Annee'].astype('Int64')

    return df_evolution



# Fonction associee a la page principale
def homepage_render(request):
    page = render(request, "homepage.html")
    return page



# Fonction associee a la page de redirection
def redirection_render(request):
    page = render(request, "redirection.html")
    return page



# Fonction associee a la page de prevision
def prevision(request):
    # Recuperation des donnees 
    df_evolution = read_csv_music_genre()

    # Colonnes pertinentes pour la prédiction
    features = ['danceability', 'energy', 'loudness', 'speechiness', 'acousticness', 'popularity', 'music_genre']
    df_evolution = df_evolution.dropna(subset=features)

    df_evolution_predict = df_evolution[features]
    
    # Modele de prediction : regression lineaire
    X = df_evolution_predict[features[:-2]]
    y = df_evolution_predict['popularity']
    nan_values_in_X = X.isnull().any()
    print("Valeurs NaN dans X :")
    print(nan_values_in_X)
    model = LinearRegression()
    model.fit(X, y)

    aggregated_data = df_evolution_predict.groupby('music_genre').mean().reset_index()

    X_genres = aggregated_data[features[:-2]] 
    y_genres = aggregated_data['popularity']

    model.fit(X_genres, y_genres)

    # Comparaison 2023 - 2024 pour chaque genre
    genres = df_evolution['music_genre'].unique()
    popularity_2023 = df_evolution[df_evolution['Annee'] == 2023].groupby('music_genre')['popularity'].mean()
    popularity_2024_predicted =  model.predict(X_genres)

    # Nettoyage pour que les deux aient la meme taille
    genres_to_remove = ['Jazz', 'Classical', 'Anime']
    popularity_2024_predicted = [pop for genre, pop in zip(genres, popularity_2024_predicted) if genre not in genres_to_remove]
    genres = genres[~np.isin(genres, genres_to_remove)]
        
    # Graphique
    bar_positions = np.arange(len(genres))
    bar_width = 0.35
    plt.figure(figsize=(12, 8))
    plt.bar(bar_positions, popularity_2023, bar_width, color='blue', label='2023')
    plt.bar(bar_positions + bar_width, popularity_2024_predicted, bar_width, color='pink', label='Prévision 2024')
    plt.xlabel('Genre musical')
    plt.ylabel('Popularité')
    plt.title('Comparaison des prévisions de popularité pour 2023 et 2024 par genre musical')
    plt.xticks(bar_positions + bar_width / 2, genres, rotation=90)

    # Valeurs sur les barres
    for i, value in enumerate(popularity_2023):
        plt.text(bar_positions[i], value, str(round(value, 2)), ha='center', va='bottom')

    for i, value in enumerate(popularity_2024_predicted):
        plt.text(bar_positions[i] + bar_width, value, str(round(value, 2)), ha='center', va='bottom')

    plt.legend()
    plt.tight_layout()

    # Recuperation du graphique pour l'afficher sur la page html
    image = get_graph()
    context = {
        'image': image,
    }

    page = render(request, "prevision.html", context)
    return page



# Fonction associee a la page de l'evolution de la musique
def evolution(request):
    # Recuperation des donnees
    df_evolution = read_csv_music_genre()

    # Nouvelle colonne pour les intervalles de 5 ans
    df_evolution['Annee_interval'] = pd.cut(df_evolution['Annee'], bins=range(1958, 2023+3, 5))

    # Calcul du genre le plus populaire pour chaque intervalle de 5 ans
    grouped = df_evolution.groupby(['Annee_interval', 'music_genre']).size().reset_index(name='count')
    popular_genre_per_interval = grouped.sort_values(['Annee_interval', 'count'], ascending=[True, False]).groupby('Annee_interval').head(1)[['Annee_interval', 'music_genre']]

    popular_genre_per_interval = popular_genre_per_interval.reset_index(drop=True)

    # Graphique (tableau)
    plt.figure(figsize=(10, 6))
    plt.axis('off')

    table = plt.table(cellText=popular_genre_per_interval.values,
                    colLabels=popular_genre_per_interval.columns,
                    cellLoc='center',
                    loc='center',
                    colColours=['#f3f3f3']*len(popular_genre_per_interval.columns),
                    cellColours=[['#f3f3f3']*len(popular_genre_per_interval.columns)]*len(popular_genre_per_interval),
                    colWidths=[0.2]*len(popular_genre_per_interval.columns))

    table.auto_set_font_size(False)
    table.set_fontsize(10)
    table.scale(1, 1.5)
    plt.tight_layout()

    # Envoi du premier graphique
    image_inter_top = get_graph()

    
    # Deuxieme graphique : comparaison par annees de la duree des musiques (en min)

    average_duration = df_evolution.groupby('Annee')['Track Duration (ms)'].mean() / (1000 * 60)

    plt.figure(figsize=(10, 6)) 
    plt.plot(average_duration.index, average_duration.values, marker='o', linestyle='-', color='purple', label='Durée moyenne des pistes')
    plt.title('Évolution de la durée moyenne des pistes par année')
    plt.xlabel('Année')
    plt.ylabel('Durée moyenne des pistes (min)')

    plt.xticks(range(1958, 2024, 6))  # Affichage tous les 6 ans
    plt.xlim(1958, 2023)
    plt.ylim(2, 5)
    plt.grid(True)
    plt.legend()
    plt.tight_layout()

    image_duree = get_graph()


    # Calcul de la popularité moyenne des genres par année
    genre_popularity = df_evolution.groupby(['Annee', 'music_genre'])['Popularity'].mean().reset_index(name='Mean_Popularity')

    plt.figure(figsize=(20, 11))
    sns.scatterplot(data=genre_popularity, x='Annee', y='music_genre', hue='Mean_Popularity', size='Mean_Popularity', sizes=(50, 200))
    plt.title('Moyenne de popularite par genre de musique et par année')
    plt.xlabel('Année')
    plt.ylabel('Genre de musique')
    plt.legend(title='Moyenne de popularite', bbox_to_anchor=(1.05, 1), loc='upper left')

    # Afficher les années par intervalles de 5 ans sur l'axe x
    plt.xticks(range(min(genre_popularity['Annee']), max(genre_popularity['Annee'])+1, 5))

    image_pop_genre = get_graph()

    
    context = {
        'image_inter_top': image_inter_top,
        'image_pop_genre': image_pop_genre, 
        'image_duree': image_duree
    }

    return render(request, "evolution.html", context)


# Fonction associee a la page de l'actualite de la musique
def actuelle(request):

    # Recuperation des donnees dans les fichiers csv
    csv_path1 = 'mysite/data/spotify-2023.csv'
    csv_path2 = 'mysite/data/best-song.csv'

    try:
        df_spotify_2023 = pd.read_csv(csv_path1, sep=';')
        print(df_spotify_2023.columns[0])
        df_best_songs = pd.read_csv(csv_path2, sep=';')
        print(df_best_songs.columns[0])
    except pd.errors.ParserError as e:
        print(f"Erreur de lecture du fichier CSV : {e}")

    # Tri des données par la colonne de popularité : top 10 des chansons
    top_10_songs = df_spotify_2023.nlargest(10, 'in_spotify_charts')

    top_10_artistes = df_spotify_2023['artist(s)_name'].value_counts().nlargest(10).reset_index()
    top_10_artistes.columns = ['Artiste', 'Nombre']

    context = {}

    # Graphique
    plt.figure(figsize=(9, 7))
    sns.barplot(x='track_name', y='in_spotify_charts', data=top_10_songs, palette='ch:s=-.2,r=.6')
    plt.title('Top 10 des Chansons sur Spotify')
    plt.xlabel('Chanson')
    plt.ylabel('Popularité dans les Charts Spotify')
    plt.xticks(rotation=45, ha='right')
    plt.tight_layout()

    # Recuperation du graphique sous format image (adaptee pour le site)
    image_songs = BytesIO()
    plt.savefig(image_songs, format='png')
    image_songs.seek(0)
    img_data_songs = base64.b64encode(image_songs.getvalue()).decode('utf-8')
    plt.close()
    context['img_data_songs'] = img_data_songs


    # Comparaison par artiste
    columns_of_interest = ['artist(s)_name', 'streams']
    df_artists_streams = df_spotify_2023[columns_of_interest]

    df_artists_streams['streams'] = df_artists_streams['streams'].replace({',': ''}, regex=True)
    df_artists_streams['streams'] = pd.to_numeric(df_artists_streams['streams'], errors='coerce')

    df_top_artists = df_artists_streams.groupby('artist(s)_name').sum()

    df_top_artists = df_top_artists.sort_values(by='streams', ascending=False)

    # Top 10
    top_10_artists_streams = df_top_artists.head(10)
    
    # Graphique
    plt.figure(figsize=(10, 6))
    sns.barplot(x=top_10_artists_streams.index, y=top_10_artists_streams['streams'], palette= 'ch:s=-.2,r=.6')
    plt.title('Top 10 Artists by Total Streams')
    plt.xlabel('Artist')
    plt.ylabel('Total Streams')
    plt.xticks(rotation=45, ha='right')
    plt.tight_layout()

    # Convertion en image
    image_top_artists = BytesIO()
    plt.savefig(image_top_artists, format='png')
    image_top_artists.seek(0)
    img_data_top_artists = base64.b64encode(image_top_artists.getvalue()).decode('utf-8')
    plt.close()
    context['img_data_top_artists'] = img_data_top_artists


    # Graphique pour le top 10 des artistes
    plt.figure(figsize=(9, 5))
    sns.barplot(x='Nombre', y='Artiste', data=top_10_artistes, palette='ch:s=-.2,r=.6')
    plt.xlabel('Nombre de Chansons')
    plt.ylabel('Artiste')
    
    image_artists = BytesIO()
    plt.savefig(image_artists, format='png')
    image_artists.seek(0)
    img_data_artists = base64.b64encode(image_artists.getvalue()).decode('utf-8')
    plt.close()
    context['img_data_artists'] = img_data_artists

    # Fusion des deux DataFrames sur le nom de l'artiste pour obtenir le genre
    df = pd.merge(df_spotify_2023, df_best_songs, left_on='artist(s)_name', right_on='artist', how='inner')

    # Meilleurs genres en fonction de la popularité dans les charts Spotify
    top_genres = df.groupby('top genre')['in_spotify_charts'].sum().nlargest(10).reset_index()

    # Graphique pour les meilleurs genres
    plt.figure(figsize=(12, 6))
    sns.barplot(x='top genre', y='in_spotify_charts', data=top_genres, palette='ch:s=-.2,r=.6')
    plt.title('Top 10 des Meilleurs Genres sur Spotify')
    plt.xlabel('Genre')
    plt.ylabel('Popularité dans les Charts Spotify')
    plt.xticks(rotation=45, ha='right')
    plt.tight_layout()

    # Enregistrement de l'image
    image_genres = BytesIO()
    plt.savefig(image_genres, format='png')
    image_genres.seek(0)
    img_data_genres = base64.b64encode(image_genres.getvalue()).decode('utf-8')
    plt.close()
    context['img_data_genres'] = img_data_genres

    
    # Nouvelle colonne 'Mode' pour identifier si la chanson est en mode Majeur ou Mineur
    df['Mode'] = df['mode'].map({'Major': 'Majeur', 'Minor': 'Mineur'})

    # Graphique
    plt.figure(figsize=(10, 6))
    sns.boxplot(x='Mode', y='in_spotify_charts', data=df, palette='ch:s=-.2,r=.6')
    plt.title('Top Hits : Distribution de la Popularité entre les Modes Majeur et Mineur')
    plt.xlabel('Mode')
    plt.ylabel('Popularité dans les Charts Spotify')
    plt.tight_layout()

    # Enregistrement de l'image
    image_boxplot = BytesIO()
    plt.savefig(image_boxplot, format='png')
    image_boxplot.seek(0)
    img_data_boxplot = base64.b64encode(image_boxplot.getvalue()).decode('utf-8')
    plt.close()
    context['img_data_boxplot'] = img_data_boxplot

    # Graphique pour les clés
    plt.figure(figsize=(12, 8))
    sns.boxplot(x='key', y='in_spotify_charts', data=df, palette='ch:s=-.2,r=.6')
    plt.title('Top Hits : Distribution de la Popularité par Clé Musicale')
    plt.xlabel('Clé Musicale')
    plt.ylabel('Popularité dans les Charts Spotify')
    plt.tight_layout()

    # Enregistrement de l'image
    image_boxplot_keys = BytesIO()
    plt.savefig(image_boxplot_keys, format='png')
    image_boxplot_keys.seek(0)
    img_data_boxplot_keys = base64.b64encode(image_boxplot_keys.getvalue()).decode('utf-8')
    plt.close()

    context['img_data_boxplot_keys'] = img_data_boxplot_keys

    
    # Graphique : Distribution des Tempos (BPM) des Chansons
    plt.figure(figsize=(9, 5))
    sns.histplot(df_spotify_2023['bpm'], bins=15, kde=True, color='black')
    plt.title('Distribution des Tempos (BPM) des Chansons')
    plt.xlabel('BPM')
    plt.ylabel('Fréquence')

    # Enregistrement de l'image
    image_distrib_tempos = BytesIO()
    plt.savefig(image_distrib_tempos, format='png')
    image_distrib_tempos.seek(0)
    img_data_distrib_tempos = base64.b64encode(image_distrib_tempos.getvalue()).decode('utf-8')
    plt.close()
    context['img_data_distrib_tempos'] = img_data_distrib_tempos
    
    return render(request, 'actuelle.html', context)